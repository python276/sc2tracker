from django.contrib import admin
from replays.models import Replay, Player


class ReplayAdmin(admin.ModelAdmin):
    list_display=('__unicode__', 'map_name', 'view_link')
    exclude=('name', 'map_name', 'date_time')

    def save_model(self, request, obj, form, change):
        super(ReplayAdmin, self).save_model(request, obj, form, change)
        obj.parse_replay()


admin.site.register(Replay, ReplayAdmin)
