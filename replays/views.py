from django.shortcuts import render_to_response, render, redirect
from django.http import Http404
from replays.models import Replay
from django.forms import ModelForm
from .models import Replay
from django.views.decorators.csrf import csrf_protect

import parser

def replays_list(request):
    return render_to_response('list.html', {'replays': Replay.objects.order_by('-id').all()})


def replay_view(request, id):
    replay = None

    try:
        replay = Replay.objects.get(pk=id)
    except:
        raise Http404

    return render_to_response('replay.html', {'replay': replay})


class ReplayForm(ModelForm):
    class Meta:
        model = Replay
        fields = ('replay',)


@csrf_protect
def replay_upload(request):
    replay = Replay()

    if request.method == 'POST':
        form = ReplayForm(request.POST, request.FILES, instance=replay)

        if form.is_valid():
            form.save()
            replay.parse_replay()
            return redirect('/replay/')
    else:
        form = ReplayForm()

    c = {
        'form': form
    }

    return render(request, 'upload.html', c)

def home(request):
    return redirect('/replay/')
