import sys
from s2protocol import s2protocol
from s2protocol.mpyq import mpyq
from s2protocol import protocol15405
import json

def parse(replay_path):
    data = {'players': []}

    # Read the protocol header, this can be read with any protocol
    archive = mpyq.MPQArchive(replay_path)

    contents = archive.header['user_data_header']['content']
    header = protocol15405.decode_replay_header(contents)

    # The header's baseBuild determines which protocol to use
    baseBuild = header['m_version']['m_baseBuild']
    try:
        protocol = __import__('protocol%s' % (baseBuild,))
    except:
        return

    # Players list
    contents = archive.read_file('replay.details')
    details = protocol.decode_replay_details(contents)

    for p in details['m_playerList']:
        player = {}

        if '<sp/>' in p['m_name']:
            player['name'] = p['m_name'].split('<sp/>', 2)[1]
        else:
            player['name'] = p['m_name']

        player['color'] = p['m_color']
        player['race'] = p['m_race']
        player['win'] = p['m_result'] == 1
        data['players'].append(player)

    data['map'] = details['m_title']

    # Initial Data (Clan tag, highest league)
    contents = archive.read_file('replay.initData')
    details = protocol.decode_replay_initdata(contents)

    for row in details['m_syncLobbyState']['m_userInitialData']:
        if row['m_observe'] == 1:
            next

        player = None

        for i in range(0, len(data['players'])):
            if data['players'][i]['name'] == row['m_name']:
                player = i
                break

        if player >= 0:
            data['players'][player]['league'] = row['m_highestLeague']
            data['players'][player]['clan'] = row['m_clanTag']

    return data

