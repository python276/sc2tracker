from __future__ import unicode_literals
from django.db import models
from django.utils.safestring import mark_safe
from replays.parser import parse


class Replay(models.Model):
    name = models.CharField(max_length=255)
    replay = models.FileField(upload_to='media/uploads/%Y/%m/%d/')
    date_time = models.DateTimeField(null=True)
    map_name = models.CharField(max_length=255, null=True)

    def __unicode__(self):
        return self.name

    def view_link(self):
        return mark_safe('<a target="_blank" href="/replay/%s/details/">%s</a>' % (self.pk, 'view'))

    def parse_replay(self):
        if  self.name:
            return

        data = parse(self.replay.path)
        self.map_name = data['map']

        parts = []

        for i in range(0, len(data['players'])):
            p = Player()
            p.name = data['players'][i]['name']
            p.race = data['players'][i]['race']
            p.win = data['players'][i]['win']
            p.color = "rgba({0}, {1}, {2}, {3})".format(
                    data['players'][i]['color']['m_r'],
                    data['players'][i]['color']['m_g'],
                    data['players'][i]['color']['m_b'],
                    data['players'][i]['color']['m_a']
            )

            if data['players'][i].has_key('clan'):
                p.clan = data['players'][i]['clan']
            else:
                p.clan = ''

            if data['players'][i].has_key('league'):
                p.league = data['players'][i]['league']
            else:
                p.league = -1

            self.players.add(p, bulk=False)

            if not p.clan:
                parts.append("{0} ({1})".format(p.name, p.race))
            else:
                parts.append("[{0}] {1} ({2})".format(p.clan, p.name, p.race))

        self.name = " - ".join(parts)
        self.save()


class Player(models.Model):
    name = models.CharField(max_length=255)
    win = models.BooleanField()
    clan = models.CharField(max_length=255, null=True)
    race = models.CharField(max_length=255)
    league = models.IntegerField()
    replay = models.ForeignKey(Replay, related_name='players', on_delete=models.CASCADE)
    color = models.CharField(max_length=255)

