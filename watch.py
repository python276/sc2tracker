import sys
import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from watchdog.events import FileCreatedEvent
import os
from subprocess import call

class SassHandler(FileSystemEventHandler):
    SASS_DIR = '/Users/overdrone/Desktop/sc2/tracker/replays/static/replays/css'
    def dispatch(self, event):
        if type(event) !=  FileCreatedEvent:
            return

        if event.src_path.endswith('.scss'):
            _cwd = os.getcwd()
            os.chdir(self.SASS_DIR)
            (name, ext) = os.path.splitext(os.path.basename(event.src_path))
            sass = '/usr/local/bin/sass'
            print "Stylesheet updated: " + name+ext+':'+name+'.css'
            call([sass, 'style.scss:style.css'])
            print "Done"
            os.chdir(_cwd)


if __name__ == "__main__":
    path = sys.argv[1] if len(sys.argv) > 1 else '.'
    event_handler = SassHandler()
    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
            observer.stop()
    observer.join()
